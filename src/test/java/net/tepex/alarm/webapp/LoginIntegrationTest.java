package net.tepex.alarm.webapp;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

import org.junit.Test;

import net.tepex.alarm.commons.UserAnswer;
import net.tepex.alarm.commons.resources.User;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class LoginIntegrationTest extends Base
{
	@Test
	public void testLogin() throws Exception
	{
		UserAnswer userAnswer = api.login("admin", "admin");
		log.info("login user: "+userAnswer.getUser());
		for(int i = 0; i < userAnswer.getGroups().length; ++i) log.info("group "+userAnswer.getGroups()[i]);
		assertThat(userAnswer.getUser(), is(notNullValue()));
		User dbUser = UserCache.getInstance().read(1L);
		assertThat(dbUser, is(userAnswer.getUser()));
	}
	
	private static final Logger log = getLogger(LoginIntegrationTest.class);
}