package net.tepex.alarm.webapp;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

import org.junit.Test;

import net.tepex.alarm.commons.PingAnswer;
import net.tepex.alarm.commons.Rest;

import net.tepex.alarm.commons.resources.User;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class PingIntegrationTest extends Base
{
	@Test
	public void test() throws Exception
	{
		Random rand = new Random();
		long userId = 1;
		double latitude = rand.nextDouble();
		double longitude = rand.nextDouble();
		PingAnswer pingAnswer = api.ping(userId, 0L, latitude, longitude);
		
		User user = UserCache.getInstance().read(userId);
		assertThat(user.getLatitude(), is(latitude));
		assertThat(user.getLongitude(), is(longitude));
	}
	
	private static final Logger log = getLogger(PingIntegrationTest.class);
}