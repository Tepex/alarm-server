package net.tepex.alarm.webapp;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

import java.io.InputStreamReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.tepex.alarm.commons.Rest;

import net.tepex.alarm.commons.UserAnswer;
import net.tepex.alarm.commons.resources.User;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class RegistrationIntegrationTest extends Base
{
	@Before
	public void setUp() throws Exception
	{
		ClassLoader cl = getClass().getClassLoader();
		InputStreamReader reader = new InputStreamReader(cl.getResourceAsStream("register.json"), "UTF-8");
		user = Rest.getGson().fromJson(reader, User.class);
	}
	
	@After
	public void tearDown() throws Exception
	{
		UserCache.getInstance().delete(user.getId());
	}
	
	@Test
	public void test() throws Exception
	{
		UserAnswer userAnswer = api.register(user);
		user.setId(userAnswer.getUser().getId());
		assertThat(userAnswer.getUser(), is(user));
	}
	
	private User user;
	private static final Logger log = getLogger(RegistrationIntegrationTest.class);
}