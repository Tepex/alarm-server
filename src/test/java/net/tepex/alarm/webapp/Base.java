package net.tepex.alarm.webapp;

import java.sql.Connection;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;

import net.tepex.alarm.commons.Rest;
import net.tepex.alarm.commons.RestApi;

import org.apache.commons.dbcp.BasicDataSourceFactory;

public class Base
{
	public Connection getConnection() throws Exception
	{
		if(connection != null) return connection;
		
		ClassLoader cl = getClass().getClassLoader();
		Properties properties = new Properties();
		properties.loadFromXML(cl.getResourceAsStream("db.xml"));
		DataSource dataSource = BasicDataSourceFactory.createDataSource(properties);
		connection = dataSource.getConnection();
		return connection;
	}
	
	protected RestApi api = Rest.getApi(Rest.LOCAL);
	private Connection connection;
}
