package net.tepex.alarm.webapp;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.tepex.alarm.commons.resources.User;
import net.tepex.alarm.webapp.UserCache;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class FriendsIntegrationTest extends Base
{
	@Before
	public void setUp() throws Exception
	{
		conn = getConnection();
	}
	
	@After
	public void tearDown() throws SQLException
	{
		conn.close();
	}
	
	@Test
	public void testGetFriends() throws Exception
	{
		addTestFriends();
		
		/* проверка */
		User[] friends;
		try
		{
			friends = api.users(USER_ID, GROUP_ID, false, 0);
		}
		catch(Exception e)
		{
			try
			{
				deleteTestFriends();
			}
			catch(Exception e1)
			{
				throw e1;
			}
			throw e;
		}
		long resultHash = 0;
		long testHash = 0;
		for(int i = 0; i < friends.length; ++i)
		{
			resultHash += friends[i].getId();
			testHash += TEST_FRIENDS[i];
		}
		
		AssertionError error = null;
		try
		{
			assertThat(testHash, is(equalTo(resultHash)));
		}
		catch(AssertionError ae)
		{
			error = ae;
		}
		
		deleteTestFriends();
		
		if(error != null) throw error;
	}
	
	/**
	 * Добавление пользователя {2} в друзья к пользователю {1} через API
	 * и проверка его наличия в БД.
	 */
	@Test
	public void testAddFriend() throws Exception
	{
		User friend = api.addFriend(USER_ID, FRIEND_ID, GROUP_ID);
		
		StringBuilder sql = new StringBuilder("SELECT `")
				.append(UserCache.COL_FRIEND_ID)
				.append("` FROM `")
				.append(UserCache.TABLE_FRIENDS)
				.append("` WHERE `")
				.append(UserCache.COL_USER_ID)
				.append("`=? AND `")
				.append(UserCache.COL_GROUP_ID)
				.append("`=?");
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, USER_ID);
		ps.setLong(2, GROUP_ID);
		ResultSet rs = ps.executeQuery();
		boolean isOk = false;
		if(rs.next()) isOk = (rs.getLong(1) == friend.getId());
		rs.close();
		ps.close();
		
		sql = new StringBuilder("DELETE FROM `")
				.append(UserCache.TABLE_FRIENDS)
				.append("` WHERE `")
				.append(UserCache.COL_USER_ID)
				.append("`=? AND `")
				.append(UserCache.COL_FRIEND_ID)
				.append("`=?");
		ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, USER_ID);
		ps.setLong(2, friend.getId());
		ps.executeUpdate();
		ps.close();

		assertThat(isOk, is(true));
	}
	
	/**
	 * Добавление друзей в БД, удаление их с помощью API
	 * и проверка их отсутствия.
	 */
	@Test
	public void testDeleteFriends() throws Exception
	{
		addTestFriends();
		
		api.deleteFriends(USER_ID, TEST_FRIENDS);
		
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM `")
				.append(UserCache.TABLE_FRIENDS)
				.append("` WHERE `")
				.append(UserCache.COL_USER_ID)
				.append("`=? AND `")
				.append(UserCache.COL_FRIEND_ID)
				.append("` IN (");
		for(int i = 0; i < TEST_FRIENDS.length; ++i)
		{
			if(i > 0) sql.append(',');
			sql.append('?');
		}
		sql.append(')');
				
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, USER_ID);
		for(int i = 0; i < TEST_FRIENDS.length; ++i) ps.setLong(i+2, TEST_FRIENDS[i]);
		ResultSet rs = ps.executeQuery();
		rs.next();
		int count = rs.getInt(1);
		rs.close();
		ps.close();
		
		assertThat(count, is(equalTo(0)));
	}
	
	/**
	 * Создание искусственных друзей в БД. 
	 */
	private void addTestFriends() throws Exception
	{
		StringBuilder sql = new StringBuilder("INSERT INTO `")
				.append(UserCache.TABLE_FRIENDS)
				.append("`(`")
				.append(UserCache.COL_USER_ID)
				.append("`, `")
				.append(UserCache.COL_FRIEND_ID)
				.append("`, `")
				.append(UserCache.COL_GROUP_ID)
				.append("`, `")
				.append(UserCache.COL_ACCEPTED)
				.append("`) VALUES ");
		/* У пользователя {10} в группе {1} есть друзья {8,12,17} */
		for(int i = 0; i < TEST_FRIENDS.length; ++i)
		{
			if(i > 0) sql.append(",");
			sql.append("(?,?,?,TRUE)");
		}
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		for(int i = 0; i < TEST_FRIENDS.length; ++i)
		{
			ps.setLong(i*3+1, USER_ID);
			ps.setLong(i*3+2, TEST_FRIENDS[i]);
			ps.setLong(i*3+3, GROUP_ID);
		}
		ps.executeUpdate();
		ps.close();
	}
	
	/**
	 * Удаление искусственных друзей из БД. 
	 */
	private void deleteTestFriends() throws Exception
	{
		StringBuilder sql = new StringBuilder("DELETE FROM `")
				.append(UserCache.TABLE_FRIENDS)
				.append("` WHERE `")
				.append(UserCache.COL_USER_ID)
				.append("`=")
				.append(USER_ID)
				.append(" AND `")
				.append(UserCache.COL_GROUP_ID)
				.append("`=")
				.append(GROUP_ID)
				.append(" AND `")
				.append(UserCache.COL_FRIEND_ID)
				.append("` IN (");
		for(int i = 0; i < TEST_FRIENDS.length; ++i)
		{
			if(i > 0) sql.append(",");
			sql.append(TEST_FRIENDS[i]);
		}
		sql.append(")");
		
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.executeUpdate();
		ps.close();
	}
	
	private static final long USER_ID = 10;
	private static final long GROUP_ID = 1;
	private static final long FRIEND_ID = 2;
	
	private static final long[] TEST_FRIENDS = {17, 12, 8};
	
	private Connection conn;
	
	private static final Logger log = getLogger(FriendsIntegrationTest.class);
}