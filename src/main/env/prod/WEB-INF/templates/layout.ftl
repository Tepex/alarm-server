<#macro tmpl title="FreeMarker example">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
	<head>
		<title>${title}</title>
		<meta http-equiv="Content-Type" content="text/xml; charset=UTF-8" />
		<link rel="stylesheet" href="/alarm/inc/style.css" type="text/css">
<#if ya??>
	<#include "ya_js.ftl"/>
</#if>
	</head>
<#assign timeout=15/>
	<body onload="timedRefresh(${timeout*1000});">
		<div id="all">
			<div class="head">
				<div><a href="index.html">Админка</a></div>
<#if logout_href??>
				<div><a href="${logout_href}">Выход</a></div>
</#if>
			</div>
			<div id="container">
				<div id="inner">
					<div id="left">left</div>
					<div id="center"><#nested/></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="foot">Footer. Reload timeout: ${timeout} sec.</div>
		</div>
	</body>
</html>
</#macro>