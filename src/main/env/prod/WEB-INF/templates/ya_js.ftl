		<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script type="text/javascript">
ymaps.ready(init);
var myMap<#list users as user>,placemark${user_index+1}</#list>;

function init()
{
	myMap = new ymaps.Map("map",
		{
			center: [55.76, 37.64],
			zoom: 11
		});
<#list users as user>
	<#assign i=user_index+1/>
	placemark${i} = new ymaps.Placemark([${user.latitude?c}, ${user.longitude?c}],
		{
			iconContent: "${user.id}",
			hintContent: "${user.name}",
			balloonContent: "${user.name}<br/>location: ${user.latitude}:${user.longitude}"
		},
		{
			iconColor:<#if user.bot> "#888888"<#else> "#0000ff"</#if>
		});
	myMap.geoObjects.add(placemark${i});
</#list>
}

function timedRefresh(timeoutPeriod)
{
	setTimeout("location.reload(true);",timeoutPeriod);
}
</script>
