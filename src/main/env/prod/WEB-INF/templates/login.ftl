<@layout.tmpl title="Авторизация">
	<div><h1>Авторизация</h1></div>
<#if (form.err)!false>
	<div class="err">Не верный логин или пароль!</div>
</#if>
	<form method="post">
		<fieldset>
			<div>
				<label for="${form.login}">Логин:</label>
				<input type="text" name="${form.login}" id="${form.login}" />
			</div>
			<div>
				<label for="${form.pwd}">Пароль:</label>
				<input type="password" name="${form.pwd}" id="${form.pwd}" />
			</div>
		</fieldset>
		<input type="submit" name="submit" value="Войти" />
	</form>

</@layout.tmpl>
