package net.tepex.alarm.webapp;

import java.util.List;

public class Utils
{
	public static boolean isNotNull(String txt)
	{
		return txt != null && txt.trim().length() > 0 ? true: false;
	}
	
	public static String unsplit(List<String> array)
	{
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < array.size(); ++i)
		{
			if(i > 0) sb.append(", ");
			sb.append(array.get(i));
		}
		return sb.toString();
	}
	
	public static boolean equals(Object obj1, Object obj2)
	{
		if(obj1 == null && obj2 == null) return true;
		if(obj1 == null) return false;
		return obj1.equals(obj2);
	}
	
	public static UserCache getUserCache()
	{
		return UserLazy.singleton;
	}
	
	public static MessageCache getMessageCache()
	{
		return MessageLazy.singleton = new MessageCache();
	}
	
	private static class UserLazy
	{
		public static UserCache singleton = new UserCache();
	}
	
	private static class MessageLazy
	{
		public static MessageCache singleton = new MessageCache();
	}
}