package net.tepex.alarm.webapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import net.tepex.alarm.commons.resources.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class MessageCache
{
	public MessageCache()
	{
	}
	
	public static MessageCache getInstance()
	{
		return Builder.INSTANCE;
	}

	public long create(Message message) throws AlarmException
	{
		sql = "INSERT INTO `"+TABLE_NAME+"`("+SQL_COLUMNS+") VALUES(?,?,?,?,NULL)";
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setLong(1, message.getFrom());
			ps.setLong(2, message.getTo());
			ps.setString(3, message.getText());
			ps.setString(4, SQL_DF.format(message.getDate()));
			ps.executeUpdate();
			ps.close();
			
			sql = "SELECT LAST_INSERT_ID()";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			rs.next();
			long id = rs.getLong(1);
			rs.close();
			ps.close();
			connection.close();
			message.setId(id);
			return id;
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	public Message read(long id) throws AlarmException
	{
		sql = "SELECT `"+COL_ID+"`, "+SQL_COLUMNS+" FROM `"+TABLE_NAME+"` WHERE `"+COL_ID+"`=?";
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			Message message = get(ps);
			ps.close();
			connection.close();
			return message;
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	private Message get(PreparedStatement ps) throws SQLException
	{
		ResultSet rs = ps.executeQuery();
		Message message = null;
		if(rs.next())
		{
			Date delivered = null;
			Timestamp ts = rs.getTimestamp(6);
			if(ts != null) delivered = new Date(ts.getTime());
			
			message = new Message(
				rs.getLong(1),
				rs.getLong(2),
				rs.getLong(3),
				rs.getString(4),
				new Date(rs.getTimestamp(5).getTime()),
				delivered,
				
				// !!! ЗАГЛУШКА !!!
				true
				);
		}
		rs.close();
		return message;
	}
	
	public Message[] getList(long id, long friendId) throws AlarmException
	{
		String where = " WHERE (`"+COL_FROM+"`=? AND `"+COL_TO+"`=?) OR (`"+COL_FROM+"`=? AND `"+COL_TO+"`=?) ORDER BY `"+COL_DATE+"`";
		try
		{
			Connection connection = DBUtil.getConnection();
			List<Message> chat = new ArrayList<Message>();
			sql = "SELECT `"+COL_ID+"`, "+SQL_COLUMNS+" FROM `"+TABLE_NAME+"`"+where;
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			ps.setLong(2, friendId);
			ps.setLong(3, friendId);
			ps.setLong(4, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Date delivered = null;
				Timestamp ts = rs.getTimestamp(6);
				if(ts != null) delivered = new Date(ts.getTime());
				Message message = new Message(
					rs.getLong(1),
					rs.getLong(2),
					rs.getLong(3),
					rs.getString(4),
					new Date(rs.getTimestamp(5).getTime()),
					delivered,
					
					
					// !!! ЗАГЛУШКА !!!
					
					true);
				chat.add(message);
			}
			rs.close();
			ps.close();
			
			sql = "UPDATE `"+TABLE_NAME+"` SET `"+COL_DELIVERED+"`=NOW()"+where;
			ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			ps.setLong(2, friendId);
			ps.setLong(3, friendId);
			ps.setLong(4, id);
			ps.executeUpdate();
			ps.close();
			
			connection.close();
			return chat.toArray(new Message[0]);
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	public void delete(long[] ids) throws AlarmException
	{
		sql = "DELETE FROM `"+TABLE_NAME+"` WHERE ";
		StringBuilder where = null;
		for(long id: ids)
		{
			if(where != null) where.append(" OR ");
			else where = new StringBuilder();
			where.append("`"+COL_ID+"`=?");
		}
		sql += where;
		log.debug("delete sql: "+sql);
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql);
			int i = 0;
			for(long id: ids) ps.setLong(++i, id);
			ps.executeUpdate();
			ps.close();
			connection.close();
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	public String getSql()
	{
		return sql;
	}
	
	private String sql;
	
	private static final Logger log = getLogger(MessageCache.class);
	
	public static final String TABLE_NAME = "messages";
	public static final String COL_ID = "id";
	public static final String COL_FROM = "from";
	public static final String COL_TO = "to";
	public static final String COL_TEXT = "text";
	public static final String COL_DATE = "date";
	public static final String COL_DELIVERED = "delivered";

	public static final String SQL_COLUMNS = "`"+COL_FROM+"`, `"+COL_TO+"`, `"+COL_TEXT+"`, `"+COL_DATE+"`, `"+COL_DELIVERED+"`";
	
	public static final DateFormat SQL_DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static class Builder
	{
		public static MessageCache INSTANCE = new MessageCache();
	}
}