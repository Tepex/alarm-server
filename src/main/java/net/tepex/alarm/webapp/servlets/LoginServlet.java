package net.tepex.alarm.webapp.servlets;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import net.tepex.alarm.webapp.AlarmException;
import net.tepex.alarm.webapp.UserCache;

import net.tepex.alarm.commons.RestApi;

import net.tepex.alarm.commons.UserAnswer;
import net.tepex.alarm.commons.resources.Group;
import net.tepex.alarm.commons.resources.User;

/**
 * Обработка авторизации пользователя. GET. Параметры: login, pwd.
 * Возвращает: User
 */
@WebServlet(name="LoginServlet", urlPatterns={RestApi.RESOURCE_USER_LOGIN})
public class LoginServlet extends BaseServlet
{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		User user = null;
		String login = request.getParameter(RestApi.PARAM_LOGIN);
		String pwd = request.getParameter(RestApi.PARAM_PWD);
		log.info("login: ["+login+", "+pwd+"]");
		try
		{
			user = UserCache.getInstance().read(login, pwd);
			/* TODO: разобраться с сессиями */
			HttpSession session = request.getSession(Boolean.TRUE);
			session.setAttribute(SESSION_USER, user);
		}
		catch(AlarmException e)
		{
			log.error("login error! "+e.getMessage());
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		if(user == null) sendResponse(response, HttpServletResponse.SC_NOT_FOUND, null);
		else
		{
			Group[] groups = UserCache.getInstance().getGroups(user.getId());
			sendResponse(response, HttpServletResponse.SC_OK, new UserAnswer(user, groups));
		}
	}
	
	private static final Logger log = getLogger(LoginServlet.class);
}
