package net.tepex.alarm.webapp.servlets;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import net.tepex.alarm.webapp.AlarmException;
import net.tepex.alarm.webapp.Location;
import net.tepex.alarm.webapp.UserCache;

import net.tepex.alarm.commons.PingAnswer;
import net.tepex.alarm.commons.RestApi;

import net.tepex.alarm.commons.resources.User;

/**	
 * Обработка пинга. PUT. Параметры: {userId, groupId, latitude, longitude}
 * Возвращает:
 * { 
 *     ping: "период пинга в секундах",
 *     online: [user1, user2, ...] - массив пользоватей онлайн
 * }
 */
@WebServlet(name="PingServlet", urlPatterns={RestApi.RESOURCE_USER_PING})
public class PingServlet extends BaseServlet
{
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		long id;
		long groupId;
		double latitude;
		double longitude;
		try
		{
			id = Long.parseLong(request.getParameter(RestApi.PARAM_ID));
			groupId = Long.parseLong(request.getParameter(RestApi.PARAM_GROUP_ID));
			latitude = Double.parseDouble(request.getParameter(RestApi.PARAM_LATITUDE));
			longitude = Double.parseDouble(request.getParameter(RestApi.PARAM_LONGITUDE));
		}
		catch(Exception e)
		{
			log.error("wrong format", e);
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		/* Обновление данных пользователя */
		try
		{
			UserCache.getInstance().update(id, latitude, longitude);
		}
		catch(AlarmException e)
		{
			log.error("Ping error!", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		
		/* определение периода пинга для пользователя */
		int delay = RAND.nextInt(3)+3;
		
		User[] userList = null;
		try
		{
			userList = UserCache.getInstance().read(id, groupId, true, 0);
		}
		catch(AlarmException e)
		{
			log.error("Ping error!", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		
		sendResponse(response, HttpServletResponse.SC_OK, new PingAnswer(delay, userList));
	}
	
	private static final Random RAND = new Random();
	private static final Logger log = getLogger(PingServlet.class);
}
