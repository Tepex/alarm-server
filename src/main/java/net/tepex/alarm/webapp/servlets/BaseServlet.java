package net.tepex.alarm.webapp.servlets;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

import net.tepex.alarm.commons.Rest;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class BaseServlet extends HttpServlet
{
	protected void sendResponse(HttpServletResponse response, int statusCode, Object gsonObject) throws IOException, ServletException
	{
		response.setContentType(CONTENT_TYPE);
		response.setStatus(statusCode);
		
		String json = Rest.getGson().toJson(gsonObject);
		log.info(getClass().getSuperclass()+". Answer: "+json);

		PrintWriter writer = response.getWriter();
		writer.write(json);
		writer.flush();
		writer.close();
	}
	
	private static final Logger log = getLogger(BaseServlet.class);
	
	public static final String SESSION_USER = "user";
	public static final String CONTENT_TYPE = "application/json; charset=UTF-8";
}