package net.tepex.alarm.webapp.servlets.web;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

@WebServlet(name="StaticFilesServlet", urlPatterns={"/inc/*"})
public class StaticFilesServlet extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		if(request.getPathInfo() == null)
		{
			log.error("Path info is NULL");
			throw new ServletException("Static servlet path info is NULL");
		}
		String path = request.getServletPath()+request.getPathInfo();
		request.getRequestDispatcher("/WEB-INF"+path).forward(request, response);
    }
    
    private static final Logger log = getLogger(StaticFilesServlet.class);
}
