package net.tepex.alarm.webapp.servlets;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import net.tepex.alarm.webapp.AlarmException;
import net.tepex.alarm.webapp.Location;
import net.tepex.alarm.webapp.UserCache;

import net.tepex.alarm.commons.Rest;
import net.tepex.alarm.commons.RestApi;

import net.tepex.alarm.commons.UserAnswer;
import net.tepex.alarm.commons.resources.Group;
import net.tepex.alarm.commons.resources.User;

@WebServlet(name="UsersServlet", urlPatterns={RestApi.RESOURCE_USERS})
public class UsersServlet extends BaseServlet
{
	/**	
	 * Получение списка пользователей.
	 * Параметры: id, groupId, isOnline, special
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		long id;
		long groupId;
		boolean isOnline = Boolean.parseBoolean(request.getParameter(RestApi.PARAM_ONLINE));
		int special;
		try
		{
			id = Long.parseLong(request.getParameter(RestApi.PARAM_ID));
			groupId = Long.parseLong(request.getParameter(RestApi.PARAM_GROUP_ID));
			special = Integer.parseInt(request.getParameter(RestApi.PARAM_SPECIAL));
		}
		catch(NumberFormatException e)
		{
			log.error("no id parameter");
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		
		User[] userList = null;
		try
		{
			userList = UserCache.getInstance().read(id, groupId, isOnline, special);
		}
		catch(AlarmException e)
		{
			log.error("UserCache.read error", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		
		sendResponse(response, HttpServletResponse.SC_OK, userList);
	}
	
	/**
	 * Регистрация пользователя.
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		log.info("start registration");
		User user = null;
		try
		{
			user = Rest.getGson().fromJson(request.getReader(), User.class);
		}
		catch(Exception e)
		{
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		try
		{
			UserCache.getInstance().create(user);
		}
		catch(AlarmException e)
		{
			log.error("Registration error", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		Group[] groups = UserCache.getInstance().getGroups(user.getId());
		sendResponse(response, HttpServletResponse.SC_OK, new UserAnswer(user, groups));
	}
	
	private static final Logger log = getLogger(UsersServlet.class);
}
