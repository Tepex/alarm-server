package net.tepex.alarm.webapp.servlets.web.admin;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import net.tepex.alarm.commons.resources.User;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class BaseAdminServlet extends HttpServlet
{
	protected User checkAuthorization(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute(SESSION_ADMIN);
		if(user == null) response.sendRedirect(AdminLoginServlet.URI);
		return user;
	}
	
	public static final String SESSION_ADMIN = "admin";
	
	private static final Logger log = getLogger(BaseAdminServlet.class);
}
