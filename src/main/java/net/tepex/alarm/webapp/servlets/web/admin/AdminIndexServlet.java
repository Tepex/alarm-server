package net.tepex.alarm.webapp.servlets.web.admin;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.Set;

import net.tepex.alarm.webapp.AlarmException;
import net.tepex.alarm.webapp.UserCache;

import net.tepex.alarm.commons.resources.User;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

@WebServlet(name="AdminIndexServlet", urlPatterns={"/web/admin/index.html"})
public class AdminIndexServlet extends BaseAdminServlet
{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		User admin = checkAuthorization(request, response);
		if(admin == null) return;
		request.setAttribute(ID_ADMIN, admin);
		request.setAttribute(ID_LOGOUT, AdminLogoutServlet.URI);
		request.setAttribute("ya", "YANDEX");
		User[] users;
		try
		{
			users = UserCache.getInstance().read(0L, 0L, true, 0);
		}
		catch(AlarmException e)
		{
			log.error("Admin index error!", e);
			throw new ServletException("Admin index error!", e);
		}
		request.setAttribute(ID_USERS, users);
		request.getRequestDispatcher(TEMPLATE).forward(request, response);
    }
    
    public static final String URI = "index.html";
    public static final String TEMPLATE = "/index.ftl";
    
    public static final String ID_ADMIN = "admin";
    public static final String ID_LOGOUT = "logout_href";
    public static final String ID_USERS = "users";
    
    private static final Logger log = getLogger(AdminIndexServlet.class);
}
