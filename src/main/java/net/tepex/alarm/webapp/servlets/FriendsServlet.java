package net.tepex.alarm.webapp.servlets;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import net.tepex.alarm.commons.Rest;
import net.tepex.alarm.commons.RestApi;
import net.tepex.alarm.commons.StringResponse;

import net.tepex.alarm.commons.resources.User;

import net.tepex.alarm.webapp.AlarmException;
import net.tepex.alarm.webapp.UserCache;

/**
 * Работа с друзьями пользователя.
 * Добавить в друзья:	addFriend()		POST /friends?id=xxx&friendId=xxx&groupId=xxx
 * Удаление друзей:		deleteFriends()	DELETE /friends?id=xxx&friendId=x,x,x...
*/
@WebServlet(name="FriendsServlet", urlPatterns={RestApi.RESOURCE_FRIENDS})
public class FriendsServlet extends BaseServlet
{
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		long id;
		long friendId;
		long groupId;
		try
		{
			id = Long.parseLong(request.getParameter(RestApi.PARAM_ID));
			friendId = Long.parseLong(request.getParameter(RestApi.PARAM_FRIEND_ID));
			groupId = Long.parseLong(request.getParameter(RestApi.PARAM_GROUP_ID));
		}
		catch(NumberFormatException e)
		{
			log.error("no userId parameter or groupId or friendId");
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		
		User friend;
		try
		{
			friend = UserCache.getInstance().addFriend(id, friendId, groupId);
		}
		catch(AlarmException e)
		{
			log.error("AddFriend error!", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		sendResponse(response, HttpServletResponse.SC_OK, friend);
	}
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		long id;
		try
		{
			id = Long.parseLong(request.getParameter(RestApi.PARAM_ID));
		}
		catch(NumberFormatException e)
		{
			log.error("no id parameter");
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		String[] friendIdArray = request.getParameterValues(RestApi.PARAM_FRIEND_ID);
		if(friendIdArray == null)
		{
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		
		long[] ids = new long[friendIdArray.length];
		try
		{
			for(int i = 0; i < friendIdArray.length; ++i) ids[i] = Long.parseLong(friendIdArray[i]);
		}
		catch(NumberFormatException e)
		{
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		try
		{
			UserCache.getInstance().deleteFriends(id, ids);
		}
		catch(AlarmException e)
		{
			log.error("deleteFriends error", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		sendResponse(response, HttpServletResponse.SC_OK, new StringResponse());
	}
	
	private static final Logger log = getLogger(FriendsServlet.class);
}
