package net.tepex.alarm.webapp.servlets.web.admin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import java.util.Map;
import java.util.HashMap;

import net.tepex.alarm.commons.resources.User;

import net.tepex.alarm.webapp.AlarmException;
import net.tepex.alarm.webapp.UserCache;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

@WebServlet(name="AdminLoginServlet", urlPatterns={"/web/admin/login.html"})
public class AdminLoginServlet extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		request.setAttribute("form", getFormData(false));
		request.getRequestDispatcher(TEMPLATE).forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		String login = request.getParameter(ID_LOGIN);
		String pwd = request.getParameter(ID_PWD);
		User admin = null;
		try
		{
			admin = UserCache.getInstance().read(login, pwd);
		}
		catch(AlarmException e)
		{
			log.error("Admin login error!", e);
			throw new ServletException("Admin login error!", e);
		}
		if(admin != null)
		{
			HttpSession session = request.getSession(true);
			session.setAttribute(BaseAdminServlet.SESSION_ADMIN, admin);
			response.sendRedirect(AdminIndexServlet.URI);
		}
		else
		{
			request.setAttribute("form", getFormData(true));
			request.getRequestDispatcher(TEMPLATE).forward(request, response);
		}
	}
	
	private Map<String, Object> getFormData(boolean err)
	{
		Map<String, Object> form = new HashMap<String, Object>();
		form.put(ID_LOGIN, ID_LOGIN);
		form.put(ID_PWD, ID_PWD);
		form.put(ID_ERR, err);
		return form;
	}
	
	public static final String TEMPLATE = "/login.ftl";
	
	public static final String URI = "login.html";
	
	public static final String ID_LOGIN = "login";
	public static final String ID_PWD = "pwd";
	public static final String ID_ERR = "err";
	
	private static final Logger log = getLogger(AdminLoginServlet.class);
}
