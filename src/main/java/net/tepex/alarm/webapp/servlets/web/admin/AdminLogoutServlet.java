package net.tepex.alarm.webapp.servlets.web.admin;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

@WebServlet(name="AdminLogoutServlet", urlPatterns={"/web/admin/logout.html"})
public class AdminLogoutServlet extends BaseAdminServlet
{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		request.getSession().invalidate();
		response.sendRedirect(AdminLoginServlet.URI);
    }
    
    public static final String URI = "logout.html";
    
    private static final Logger log = getLogger(AdminLogoutServlet.class);
}
