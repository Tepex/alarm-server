package net.tepex.alarm.webapp.servlets;

import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import net.tepex.alarm.commons.Rest;
import net.tepex.alarm.commons.RestApi;

import net.tepex.alarm.commons.StringResponse;
import net.tepex.alarm.commons.resources.Message;

import net.tepex.alarm.webapp.AlarmException;
import net.tepex.alarm.webapp.MessageCache;

/**
 * Работа с чатом пользователя.
 * Выдача чата: GET /chat?id=xxx&friendId=yyy
 * Удаление собщений: DELETE /chat?id=1,2,3,...
 * Новое сообщение: POST /chat
*/
@WebServlet(name="ChatServlet", urlPatterns={RestApi.RESOURCE_CHAT})
public class ChatServlet extends BaseServlet
{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		long id;
		long friendId;
		try
		{
			id = Long.parseLong(request.getParameter(RestApi.PARAM_ID));
			friendId = Long.parseLong(request.getParameter(RestApi.PARAM_FRIEND_ID));
		}
		catch(NumberFormatException e)
		{
			log.error("no userId parameter");
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		Message[] chat;
		try
		{
			chat = MessageCache.getInstance().getList(id, friendId);
		}
		catch(AlarmException e)
		{
			log.error("GetChat error!", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		sendResponse(response, HttpServletResponse.SC_OK, chat);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		Message message;
		try
		{
			message = Rest.getGson().fromJson(request.getReader(), Message.class);
		}
		catch(Exception e)
		{
			log.error("Error code: params");
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		try
		{
			MessageCache.getInstance().create(message);
		}
		catch(AlarmException e)
		{
			log.error("New message error", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		sendResponse(response, HttpServletResponse.SC_CREATED, message);
	}
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String[] idArray = request.getParameterValues(RestApi.PARAM_ID);
		if(idArray == null)
		{
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		
		long[] ids = new long[idArray.length];
		try
		{
			for(int i = 0; i < idArray.length; ++i) ids[i] = Long.parseLong(idArray[i]);
		}
		catch(NumberFormatException e)
		{
			sendResponse(response, HttpServletResponse.SC_BAD_REQUEST, null);
			return;
		}
		try
		{
			MessageCache.getInstance().delete(ids);
		}
		catch(AlarmException e)
		{
			log.error("Del message error", e);
			sendResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
			return;
		}
		sendResponse(response, HttpServletResponse.SC_OK, new StringResponse());
	}
	
	private static final Logger log = getLogger(ChatServlet.class);
}
