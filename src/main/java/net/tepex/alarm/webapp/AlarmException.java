package net.tepex.alarm.webapp;

/**
 * Исключение если сущность уже существует в БД.
 * @author Tepex <tepex@mail.ru>
 */
public class AlarmException extends RuntimeException
{
	/**
	 * Конструктор исключения.
	 * @param msg Сообщение об ошибке.
	 */
	public AlarmException(String msg)
	{
		super(msg);
	}
	
	public AlarmException(String msg, Exception e)
	{
		super(msg, e);
	}
}