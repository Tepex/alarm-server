package net.tepex.alarm.webapp;

import java.io.InputStream;
import java.io.FileInputStream;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

import java.util.Hashtable;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSourceFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class DBUtil
{
	private DBUtil()
	{
		try
		{
			Context ctx = new InitialContext();
			dataSource = (DataSource)ctx.lookup("java:comp/env/jdbc/AlarmDB");
		}
		catch(NamingException e)
		{
			log.error("Database initialization error!");
		}
		if(dataSource == null)
		{
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			Properties properties = new Properties();
			try
			{
				properties.loadFromXML(cl.getResourceAsStream("db.xml"));
				dataSource = BasicDataSourceFactory.createDataSource(properties);
			}
			catch(Exception e)
			{
				log.error("Database initialization error!", e);
			}
		}
	}
	
	public static Connection getConnection() throws SQLException
	{
		return Lazy.singleton.dataSource.getConnection();
	}
	
	private DataSource dataSource;
	private static final Logger log = getLogger(DBUtil.class);
	
	private static class Lazy
	{
		public static DBUtil singleton = new DBUtil();
	}
}