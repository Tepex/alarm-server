package net.tepex.alarm.webapp;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;

import net.tepex.alarm.commons.resources.Group;
import net.tepex.alarm.commons.resources.User;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

/**
 *  CRUD-прослойка для пользователей между БД и представлением.
 *
 * @author Tepex <tepex@mail.ru>
 */
public class UserCache
{
	public UserCache()
	{
	}
	
	public static UserCache getInstance()
	{
		return Builder.INSTANCE;
	}
	
	/* ------ CREATE ------ */
	
	public long create(User user) throws AlarmException
	{
		log.info("user pwd: "+user.getPassword());
		sql = new StringBuilder("INSERT INTO `")
			.append(TABLE_NAME)
			.append("`(`")
			.append(COL_LOGIN)
			.append("`, `")
			.append(COL_NAME)
			.append("`, `")
			.append(COL_EMAIL)
			.append("`, `")
			.append(COL_PWD)
			.append("`, `")
			.append(COL_LATITUDE)
			.append("`, `")
			.append(COL_LONGITUDE)
			.append("`, `")
			.append(COL_LAST_ACCESS)
			.append("`, `")
			.append(COL_CREATED)
			.append("`) VALUES(?,?,?,?,?,?,NOW(),NOW())");
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLogin());
			ps.setString(2, user.getName());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getPassword());
			ps.setDouble(5, user.getLatitude());
			ps.setDouble(6, user.getLongitude());
			ps.executeUpdate();
			ps.close();
		
			ps = connection.prepareStatement("SELECT LAST_INSERT_ID()");
			ResultSet rs = ps.executeQuery();
			rs.next();
			long id = rs.getLong(1);
			rs.close();
			ps.close();
		
			sql = new StringBuilder("INSERT INTO `")
				.append(TABLE_DEVICES)
				.append("`(`")
				.append(COL_MODEL)
				.append("`, `")
				.append(COL_SERIAL)
				.append("`, `")
				.append(COL_PLATFORM)
				.append("`, `")
				.append(COL_USER_ID)
				.append("`) VALUES(?,?,?,?)");
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getModel());
			ps.setString(2, user.getSerial());
			ps.setString(3, user.getPlatform());
			ps.setLong(4, id);
			ps.executeUpdate();
			ps.close();
			connection.close();
			user.setId(id);
			return id;
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	
	/* ------ READ ------ */
	
	public User read(long id) throws AlarmException
	{
		sql = new StringBuilder(SQL_GET_USER).append(" WHERE u.`").append(COL_ID+"`=?");
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setLong(1, id);
			User user = get(ps);
			ps.close();
			connection.close();
			return user;
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	/**
	 * Поиск пользователя по логину и паролю.
	 */
	public User read(String login, String pwd) throws AlarmException
	{
		sql = new StringBuilder(SQL_GET_USER)
			.append(" WHERE u.`")
			.append(COL_LOGIN)
			.append("`=? AND u.`")
			.append(COL_PWD)
			.append("`=?");
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setString(1, login);
			ps.setString(2, pwd);
			User user = get(ps);
			ps.close();
			connection.close();
			return user;
		}			
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	/**
	 * Поиск пользователя по логину или email.
	 *
	 * @param fake ничего не делает. Нужен только чтобы различать два read(String, String) с одинаковыми полями.
	 */
	public User read(String login, String email, boolean fake) throws AlarmException
	{
		sql = new StringBuilder(SQL_GET_USER)
			.append(" WHERE u.`")
			.append(COL_LOGIN)
			.append("`=? OR u.`")
			.append(COL_EMAIL)
			.append("`=?");
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setString(1, login);
			ps.setString(2, email);
			User user = get(ps);
			ps.close();
			connection.close();
			return user;
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	/**
	 * Получение списка пользователей (друзей).
	 *
	 * @param id					 id пользователя.
	 * @param groupId			 id группы.
	 * @param isOnline			 true - выдача только онлайн-пользователей.
	 * @param special			 Если не 0, то возвращаются только спец. пользователи с типом special.
	 * @return					 массив пользователей.
	 * @exception AlarmException  выбрасывается, при ошибке SQL. 
	 */
	public User[] read(long id, long groupId, boolean isOnline, int special) throws AlarmException
	{
		Set<User> set = new HashSet<User>();
		sql = new StringBuilder(SQL_GET_USER);
		if(special > 0) sql.append(" JOIN `")
			.append(TABLE_SPECIAL)
			.append("` AS sp ON sp.`")
			.append(COL_USER_ID)
			.append("`=u.`")
			.append(COL_ID)
			.append("` AND sp.`")
			.append(COL_TYPE)
			.append("`=?");
		if(groupId > 0) sql.append(" JOIN `")
			.append(TABLE_FRIENDS)
			.append("` AS f ON f.`")
			.append(COL_FRIEND_ID)
			.append("`=u.`")
			.append(COL_ID)
			.append("` AND f.`")
			.append(COL_USER_ID)
			.append("`=? AND f.`")
			.append(COL_GROUP_ID)
			.append("`=? AND f.`")
			.append(COL_ACCEPTED)
			.append("`=TRUE");
			
		sql.append(" WHERE u.`")
			.append(COL_ID)
			.append("` <> ?");
		if(isOnline) sql.append(" AND ADDTIME(u.`").append(COL_LAST_ACCESS).append("`, '0 0:5') > NOW()");
		log.info("read: "+sql);
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			int i = 1;
			if(special > 0) ps.setInt(i++, special);
			if(groupId > 0)
			{
				ps.setLong(i++, id);
				ps.setLong(i++, groupId);
			}
			ps.setLong(i, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) set.add(init(rs));
			rs.close();
			ps.close();
			connection.close();
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
		return set.toArray(new User[0]);
	}
	 
	/**
	 * Получение друзей пользователя. Без закрытия соединения с БД.
	 */
	private User get(PreparedStatement ps) throws SQLException
	{
		ResultSet rs = ps.executeQuery();
		User user = null;
		if(rs.next()) user = init(rs);
		rs.close();
		return user;
	}
	
	/* ------ UPDATE ------ */
	
	public void update(long id, double latitude, double longitude) throws AlarmException
	{
		try
		{
			User user = read(id);
			if(user == null) return;
			Connection connection = DBUtil.getConnection();
			sql = new StringBuilder("UPDATE `")
				.append(TABLE_NAME)
				.append("` SET `")
				.append(COL_LATITUDE)
				.append("`=?, `")
				.append(COL_LONGITUDE)
				.append("`=?, `")
				.append(COL_LAST_ACCESS)
				.append("`=NOW() WHERE `")
				.append(COL_ID)
				.append("`=?");
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setDouble(1, latitude);
			ps.setDouble(2, longitude);
			ps.setLong(3, id);
			ps.executeUpdate();
			ps.close();
			connection.close();
		}
		catch(SQLException e)
		{
			log.error("ping update: "+sql);
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	/* ------ DELETE ------ */
	
	public void delete(long id) throws AlarmException
	{
		sql = new StringBuilder("DELETE FROM `")
			.append(TABLE_DEVICES)
			.append("` WHERE `")
			.append(COL_USER_ID)
			.append("`=?");
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setLong(1, id);
			ps.executeUpdate();
			ps.close();
			
			sql = new StringBuilder("DELETE FROM `")
				.append(TABLE_NAME)
				.append("` WHERE `")
				.append(COL_ID+"`=?");
			ps = connection.prepareStatement(sql.toString());
			ps.setLong(1, id);
			ps.executeUpdate();
			ps.close();			
			connection.close();
		}
		catch(SQLException e)
		{
			log.error("delete user: "+sql);
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	
	/* ------ UTILS ------ */
	
	/**
	 * Добавление в друзья.
	 *
	 * @param id					id пользователя.
	 * @param friendId			id друга.
	 * @param groupId			id группы.
	 * @return 					добавленный друг.
	 * @exception AlarmException выбрасывается, при ошибке SQL. 
	 */
	public User addFriend(long id, long friendId, long groupId) throws AlarmException
	{
		sql = new StringBuilder("INSERT INTO `")
			.append(TABLE_FRIENDS)
			.append("`(`")
			.append(COL_USER_ID)
			.append("`, `")
			.append(COL_FRIEND_ID)
			.append("`, `")
			.append(COL_GROUP_ID)
			.append("`) VALUES(?, ?, ?)");
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setLong(1, id);
			ps.setLong(2, friendId);
			ps.setLong(3, groupId);
			ps.executeUpdate();
			ps.close();
		
			sql = new StringBuilder(SQL_GET_USER).append(" WHERE u.`").append(COL_ID).append("`=?");
			ps = connection.prepareStatement(sql.toString());
			ps.setLong(1, friendId);
			User user = get(ps);
			ps.close();
			connection.close();
			return user;
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	/**
	 * Удаление из друзей.
	 *
	 * @param id					 id пользователя.
	 * @param friendsId			 список id друзей.
	 * @exception AlarmException  выбрасывается, при ошибке SQL. 
	 */
	public void deleteFriends(long id, long[] friendsId) throws AlarmException
	{
		sql = new StringBuilder("DELETE FROM `")
			.append(TABLE_FRIENDS)
			.append("` WHERE `")
			.append(COL_USER_ID)
			.append("`=? AND `")
			.append(COL_FRIEND_ID)
			.append("` IN (");
		for(int i = 0; i < friendsId.length; ++i)
		{
			if(i > 0) sql.append(',');
			sql.append('?');
		}
		sql.append(')');
		log.debug("delete sql: "+sql);
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setLong(1, id);
			for(int i = 0; i < friendsId.length; ++i) ps.setLong(i+2, friendsId[i]);
			ps.executeUpdate();
			ps.close();
			connection.close();
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
	}
	
	/**
	 * Получение списка групп пользователя.
	 *
	 * @param id
	 * @param id					 id пользователя.
	 * @exception AlarmException  выбрасывается, при ошибке SQL. 
	 */
	public Group[] getGroups(long id) throws AlarmException
	{
		Set<Group> set = new HashSet<Group>();
		sql = new StringBuilder("SELECT `")
			.append(COL_ID)
			.append("`, `")
			.append(COL_NAME)
			.append("` FROM `")
			.append(TABLE_GROUPS)
			.append("` WHERE `")
			.append(COL_USER_ID)
			.append("`=? OR `")
			.append(COL_USER_ID)
			.append("` IS NULL");
		try
		{
			Connection connection = DBUtil.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql.toString());
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) set.add(new Group(rs.getLong(1), rs.getString(2)));
			rs.close();
			ps.close();
			connection.close();
		}
		catch(SQLException e)
		{
			throw new AlarmException("Internal storage error! SQL: "+sql, e);
		}
		return set.toArray(new Group[0]);
	}
	
	private static User init(ResultSet rs) throws SQLException
	{
		return new User(
			rs.getLong(COL_ID),
			rs.getString(COL_LOGIN),
			rs.getString(COL_PWD),
			rs.getString(COL_EMAIL),
			rs.getString(COL_NAME),
			rs.getDouble(COL_LATITUDE),
			rs.getDouble(COL_LONGITUDE),
			new Date(rs.getTimestamp(COL_LAST_ACCESS).getTime()),
			new Date(rs.getTimestamp(COL_CREATED).getTime()),
			rs.getString(COL_MODEL),
			rs.getString(COL_SERIAL),
			rs.getString(COL_PLATFORM));
	}
	
	private StringBuilder sql;
	
	private static final Logger log = getLogger(UserCache.class);

	public static final String TABLE_NAME = "users";
	public static final String TABLE_DEVICES = "devices";
	public static final String TABLE_FRIENDS = "friends";
	public static final String TABLE_GROUPS = "groups";
	public static final String TABLE_SPECIAL = "special_users";
	
	public static final String COL_ID = "id";
	
	public static final String COL_MODEL = "model";
	public static final String COL_SERIAL = "serial";
	public static final String COL_PLATFORM = "platform";
	
	public static final String COL_LOGIN = "login";
	public static final String COL_PWD = "pwd";
	public static final String COL_EMAIL = "email";
	public static final String COL_NAME = "name";
	public static final String COL_LAST_ACCESS = "lastAccess";
	public static final String COL_LATITUDE = "latitude";
	public static final String COL_LONGITUDE = "longitude";
	public static final String COL_CREATED = "created";
	
	public static final String COL_USER_ID = "user_id";
	public static final String COL_FRIEND_ID = "friend_id";
	public static final String COL_GROUP_ID = "group_id";
	
	public static final String COL_ACCEPTED = "accepted";
	public static final String COL_TYPE = "type";

	public static final String SQL_GET_USER = "SELECT "+
		"u.`"+COL_ID+"`, "+
		"u.`"+COL_LOGIN+"`, "+
		"u.`"+COL_PWD+"`, "+
		"u.`"+COL_EMAIL+"`, "+
		"u.`"+COL_NAME+"`, "+
		"u.`"+COL_LATITUDE+"`, "+
		"u.`"+COL_LONGITUDE+"`, "+
		"u.`"+COL_LAST_ACCESS+"`, "+
		"u.`"+COL_CREATED+"`, "+
		"d.`"+COL_MODEL+"`, "+
		"d.`"+COL_SERIAL+"`, "+
		"d.`"+COL_PLATFORM+"` "+
		"FROM `"+TABLE_NAME+"` AS u "+
		"LEFT JOIN `"+TABLE_DEVICES+"` AS d ON u.`"+COL_ID+"`=d.`"+COL_USER_ID+"`";
		
	private static class Builder
	{
		public static UserCache INSTANCE = new UserCache();
	}
}