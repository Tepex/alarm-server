package net.tepex.alarm.webapp;

/**
 * POJO. Координаты. Широта и долгота.
 * 
 * @author Tepex <tepex@mail.ru>
 */
public class Location
{
	public Location(double latitude, double longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;
		init();
	}
	
	public Location(String lat, String lng) throws NumberFormatException
	{
		latitude = Double.parseDouble(lat);
		longitude = Double.parseDouble(lng);
		init();
	}
	
	/**
	 * Инициализация хеша и строкового представлкния.
	 */
	private void init()
	{
		/* Какое-то простое число для старта. Мой год рождения, например. */
		hash = 1979;  
		/* 101 мне тоже нравится */
		hash = hash * 101 + (int)(latitude * 1000);
		hash = hash * 101 + (int)(longitude * 1000);
		str = latitude+":"+longitude;
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	@Override
	public int hashCode()
	{
		return hash;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof Location)) return false;
		Location other = (Location)obj;
		return (latitude == other.latitude && longitude == other.longitude);
	}
	
	@Override
	public String toString()
	{
		return str; 
	}
	
	private final double latitude;
	private final double longitude;
	private int hash;
	private String str;
}