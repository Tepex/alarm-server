# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
#### Протокол  ####
~~~~
Параметры устройства передаются один раз - при регистрации
В preferences на телефоне хранится user_id и характеризует авторизацию пользователя (его сессию).
Сессия бесконечна.

Авторизация:
PUT /alarm/login
{
	login=login
	pwd=pwd
}

Ответ успешной авторизации 200 (OK):
{"userId": 12312, "ping": 100}

Или неудачная авторизация 404 (Not Found):
{"msg": "error message"}

Регистрация:
POST /alarm/register
{
	model=
	serial=
	platform=
	login=
	pwd=
	email=
	name=
	latitude=
	longitude=
}

Удачная регистрация 201 (Created): 
{"userId": 123123, "ping": 100}

Пользователь уже есть 404 (Not Found):
{"msg": "error message"}

Пинг:
PUT /alarm/ping
{
	userId = 123
	latitude=55.23423423
	longitude=38.12312312
}

Ответ. Удачный пинг 200 (OK):
{
	"ping": 100,
	"online": [...]
}

~~~~

* Команды для админа

~~~~
Получение списка ботов:
GET /alarm/getbots

Ответ 200 (ОК):
[{"id": id, "login": "bot login", "pwd": "bot pwd", "name": "bot name", "email": "qqq@qqq.ru", "isBot": true, "isAdmin": false, "lastAccess": "dd.MM.yyyy HH:mm:ss", "created": "dd.MM.yyyy HH:mm:ss", "latitude": 111, "longitude": 222, "model": "my model", "serial": "my srial", "platform": "my platform"}, {}, {}]

Ответ ошибка 404 (Not Found):
{"msg": "error msg"}
~~~~

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
Директории конфигураций находятся в /src/main/env/
На данный момент есть две конфигурации:
/src/main/env/dev
/src/main/env/prod
При сборке нужная конфигурация выбирация переменной окружения env
Сборка разработчика:
````.sh
./gradlew build -Penv=dev
````
Сборка продакшн:
````.sh
./gradlew build -Penv=prod
````

* Dependencies
* Database configuration
````.sql
SET NAMES utf8;
DROP TABLE IF EXISTS `devices`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `admins`;

CREATE TABLE IF NOT EXISTS `users` (
`id` BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
`login` VARCHAR(255) NOT NULL UNIQUE,
`pwd` VARCHAR(255) NOT NULL,
`email` VARCHAR(255) NOT NULL UNIQUE,
`name` VARCHAR(255) NOT NULL,
`isBot` BOOL NOT NULL DEFAULT '0',
`isAdmin` BOOL NOT NULL DEFAULT '0',
`lastAccess` TIMESTAMP NOT NULL,
`latitude` DOUBLE NOT NULL,
`longitude` DOUBLE NOT NULL,
`created` TIMESTAMP NOT NULL)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `devices` (
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
`model` VARCHAR(255),
`serial` VARCHAR(255),
`platform` ENUM('Android', 'iOS', 'WM', 'Other') NOT NULL),
`user_id` BIGINT UNSIGNED DEFAULT NULL,
FOREIGN KEY(`user_id`) REFERENCES users(`id`)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `admins` (
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
`login` VARCHAR(255) NOT NULL UNIQUE,
`pwd` VARCHAR(255) NOT NULL,
`name` VARCHAR(255) NOT NULL,
`created` TIMESTAMP NOT NULL)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;
````


* How to run tests
````` .sh
./gradlew integrationTest --info -S
`````
Локальные настройки БД для тестов находятся в /src/main/env/dev/db.xml
* Deployment instructions
`````.sh
./gradlew clean appRun --info -S
`````
production:
`````.sh
./gradlew cargoRedeployRemote --info -S
`````

### Contribution guidelines ###

* Writing tests

Данные для тестов находятся в файлах properties в /tests/
properties обязательно должны содержать 2 значения:
URL = адрес теста
method = POST | GET
Остальные значения - параметры запроса
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact