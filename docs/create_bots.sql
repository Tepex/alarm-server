DROP PROCEDURE IF EXISTS create_bots;
DELIMITER //
CREATE PROCEDURE `create_bots`(IN bot_count INT)
BEGIN
	DECLARE i INT;
	DECLARE bot_name, bot_login, bot_pwd, bot_email VARCHAR(255);
	DECLARE latitude, longitude DOUBLE;
	DECLARE user_id BIGINT;
	SET i = 0;

	WHILE i < bot_count DO
		SET bot_name = CONCAT('Bot ', CHAR(i+65));
		SET bot_login = CONCAT('bot_', CHAR(i+97));
		SET bot_pwd = CONCAT(bot_login, '_pwd');
		SET bot_email = CONCAT(bot_login, '@tepex.net');
		SET latitude = 55.75434 + (RAND()*2 - 1);
		SET longitude = 37.62342 + (RAND()*2 - 1);
		INSERT INTO `users`(`login`, `pwd`, `email`, `name`, `isBot`, `isAdmin`, `lastAccess`, `latitude`, `longitude`, `created`) VALUES
			(bot_login, bot_pwd, bot_email, bot_name, TRUE, FALSE, NOW(), latitude, longitude, NOW());
		SET user_id = LAST_INSERT_ID();
		INSERT INTO `devices`(`model`, `serial`, `platform`, `user_id`) VALUES
			('Bot', CONCAt('serial_', CHAR(i+97)), 'Other', user_id);
		SET i = i + 1;
	END WHILE;
END //
DELIMITER ;
