SET NAMES utf8;

DROP TABLE IF EXISTS `messages`;
DROP TABLE IF EXISTS `admins`;
DROP TABLE IF EXISTS `devices`;
DROP TABLE IF EXISTS `groups`;
DROP TABLE IF EXISTS `friends`;
DROP TABLE IF EXISTS `special_users`;
DROP TABLE IF EXISTS `users`;

CREATE TABLE `admins` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastAccess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `devices` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(255) DEFAULT NULL,
  `serial` varchar(255) DEFAULT NULL,
  `platform` enum('Android','iOS','WM','Other') NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `messages` (
	`id` bigint UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`from` bigint UNSIGNED NOT NULL,
	`to` bigint UNSIGNED NOT NULL,
	`text` varchar(2000) NOT NULL,
	`date` datetime NOT NULL,
	FOREIGN KEY(`from`) REFERENCES `users`(`id`),
	FOREIGN KEY(`to`) REFERENCES `users`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users`(`login`, `pwd`, `email`, `name`, `isAdmin`, `latitude`, `longitude`, `created`) VALUES 
('admin', 'admin', 'admin@tepex.net', 'Admin', TRUE, 55.1234, 33.333, NOW()),
('tepex', 'k155la3', 'tepex@mail.ru', 'Tepex', TRUE, 55.1234, 33.333, NOW());

INSERT INTO `devices`(`model`, `serial`, `platform`, `user_id`) VALUES
('4S', 'dfsdfsdfsdf', 'iOS', 1),
('4S', 'dfsdfsdfsdf', 'iOS', 2);

CREATE TABLE `groups` (
	`id` bigint UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`user_id` bigint UNSIGNED,
	`name` varchar(255) NOT NULL
) ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Если user_id=NULL - это общая для всех группа';

INSERT INTO `groups`(`name`) VALUES
('Друзья'),
('Коллеги'),
('Семья');

CREATE TABLE `friends` (
	`user_id` bigint UNSIGNED NOT NULL,
	`friend_id` bigint UNSIGNED NOT NULL,
	`group_id` bigint UNSIGNED NOT NULL,
	`accepted` bool NOT NULL DEFAULT FALSE,
	`added` timestamp NOT NULL,
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`),
	FOREIGN KEY(`friend_id`) REFERENCES `users`(`id`),
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`id`),
	UNIQUE(`user_id`, `friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `special_users` (
	`user_id` bigint UNSIGNED NOT NULL,
	`type` tinyint UNSIGNED NOT NULL,
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)
) ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='type: 1 - админы, 2 - роботы ';